provider "aws" {
  region                  =  var.region
  shared_credentials_file = "/home/taran/.aws/credentials"
  profile                 = "default"
}


resource "aws_s3_bucket" "bucket" {
  bucket = var.bucket_name
  acl = "private"
  versioning {
    enabled = true
  }
}