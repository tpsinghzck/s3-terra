variable "bucket_name" {
  description = "AWS Bucket Name"
  type = string
  }

# variable "name" {
#   description = "AWS Bucket tag"
#   type = string
#   }

variable "region" {
  description = "AWS Region"
  type = string
  }
